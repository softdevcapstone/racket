//
//  FirebaseFunctions.swift
//  Rackets
//
//  Created by Pau Lleonart Calvo on 4/18/21.
//

import SwiftUI
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

var userHandles = [String: UInt]()
var reviewHandles = [String: UInt]()
var reviewIdHandles = [String: UInt]()

/**
 CONSTRUCTOR FUNCTIONS
 */

/*
 Master launch function. Gets data depending on if the current user is admin or not
 */
func populateNecessaryDataOnLaunch(userData: UserData, userID: String) {
    let requestGroup = DispatchGroup()
    
    print(userID)
    
    requestGroup.enter()
    database.child("users").child(userID).observeSingleEvent(of: .value, with: {(snapshot) in
        if let userDict = snapshot.value as? [String: Any] {
            
            userData.currentUser = getDbUserFromUserDict(userDict: userDict)
            attachObserverToCurrentUser(userData: userData)
            userData.userAuthenticated = true
            
            print("*** CURRENT USER IS ADMIN ***: \(userData.currentUser.isAdmin)")
            
            requestGroup.enter()
            
            storage.child("images/\(userID).jpg").getData(maxSize: 5 * 1024 * 1024) { (data, error) in
                guard error == nil else {
                    requestGroup.leave()
                    print("*** USER IMAGE NOT FOUND ***")
                    return
                }
                
                userData.currentUser.profilePic = Image(uiImage: UIImage(data: data!)!)

                print("*** USER IMAGE WAS FOUND ***")
                requestGroup.leave()
            }
            
        }
        else {
            print("*** CURRENT USERS DATA COULD NOT BE READ FROM DATABASE ***")
        }
        
        requestGroup.leave()
    })
    
    requestGroup.notify(queue: .main) {
        if userData.currentUser.isAdmin { // Get all users' data
            adminLaunch(userData: userData)
        }
        else { // Only get current user's data
            clientLaunch(userData: userData)
        }
    }
}

/*
 Attaches an observer to the currently logged in user's information. Updates the
 userData.currentUser object in real time.
 */
func attachObserverToCurrentUser(userData: UserData) {
    userHandles[userData.currentUser.id] = database.child("users").child(userData.currentUser.id).observe(DataEventType.value, with: {(snapshot) in
        
        if let userDict = snapshot.value as? [String: Any] {
            let newUser = getDbUserFromUserDict(userDict: userDict)
            
            // Check if privileges changed. If so, log user out
            if (newUser.isAdmin != userData.currentUser.isAdmin) {
                logOutCurrentUser(userData: userData)
            }
            else {
                userData.currentUser = newUser
            }
        }
        else {
            print("*** Could not read isAdmin data as a boolean")
        }
    })
}


/**
 ADMIN SIDE FUNCTIONS
 */
func adminLaunch(userData: UserData) {
    database.child("user_ids").observe(DataEventType.value, with: {(snapshot) in
        
        if let userIds = snapshot.value as? [String: Bool] {
            
            // Handle real time local deletion of users that no longer exist
            for (userKey, _) in userData.allUsers {
                if userIds[userKey] == nil {
                    userData.allUsers.removeValue(forKey: userKey)
                    userHandles.removeValue(forKey: userKey)
                    reviewIdHandles.removeValue(forKey: userKey)
                    
                    if let reviews = userData.allReviews[userKey] {
                        for (lessonKey, _) in reviews {
                            reviewHandles.removeValue(forKey: lessonKey)
                        }
                    }
                    userData.allReviews.removeValue(forKey: userKey)
                }
            }
            
            for (userId, _) in userIds { // Key is the userId
                
                if userData.allUsers[userId] == nil && userData.currentUser.id != userId {
                    // This user is new to local dictionary and should be observed
                    attachObserverToUserWithId(userData: userData, userId: userId)
                }
            }
        }
        else {
            print("*** Could not rease user_ids as a dictionary")
        }
    })
}

func attachObserverToUserWithId(userData: UserData, userId: String) {
    
    userHandles[userId] = database.child("users").child(userId).observe(DataEventType.value, with: {(snapshot) in
        
        if let userDict = snapshot.value as? [String: Any] {
            
            // Get user and add to local user dictionary, then add to all reviews dictionary
            var newUser = getDbUserFromUserDict(userDict: userDict)
            
            if newUser.isAdmin {
                // Dont want to display admins to other admins
                if userData.allUsers[newUser.id] != nil {
                    userData.allUsers.removeValue(forKey: newUser.id)
                }
                return
            }
            
            if userData.allUsers[userId] == nil {
                let requestGroup = DispatchGroup()
                requestGroup.enter()
                
                storage.child("images/\(userId).jpg").getData(maxSize: 5 * 1024 * 1024) { (data, error) in
                    guard error == nil else {
                        requestGroup.leave()
                        print("*** USER IMAGE NOT FOUND ***")
                        return
                    }
                    
                    newUser.profilePic = Image(uiImage: UIImage(data: data!)!)
                    requestGroup.leave()
                }
                
                // Image either gotten or not gotten by this point
                requestGroup.notify(queue: .main) {
                    userData.allUsers[userId] = newUser
                    
                    // Then, attach a listener to user's reviews if userId is not in allReviews dictionary
                    userData.allReviews[userId] = [String: LessonReview]()
                    attachObserverToReviewIdsOfUser(userData: userData, userId: userId)
                }
            }
            else {
                newUser.profilePic = userData.allUsers[userId]!.profilePic
                userData.allUsers[userId] = newUser
            } // End of conditional
            
        }
        else {
            print("*** Could not read user data as a dictionary")
        }
    })
}

func attachObserverToReviewIdsOfUser(userData: UserData, userId: String) {
    
    reviewIdHandles[userId] = database.child("review_ids").child(userId).observe(DataEventType.value, with: {(snapshot) in
        
        if let reviewIds = snapshot.value as? [String: Bool] {
            
            if userData.currentUser.isAdmin { // User is admin so store things in dictionary
                for (reviewId, isComplete) in reviewIds {
                    
                    // Check if there is a local review, that is not in the new updated set of reviews
                    
                    if let userReviews = userData.allReviews[userId] {
                        for (reviewKey, _) in userReviews {
                            if reviewIds[reviewKey] == nil {
                                userData.allReviews[userId]!.removeValue(forKey: reviewKey)
                                reviewHandles.removeValue(forKey: reviewKey)
                            }
                        }
                    }
                    
                    
                    // Initialize userId local reviews dictionary
                    if userData.allReviews[userId]![reviewId] == nil {
                        // This review is new to local so it should either be gotten or observed
                        if isComplete { // Get once, dont need to observe
                            getReviewOfUser(userData: userData, userId: userId, reviewId: reviewId)
                        }
                        else { // Observe since we need to know when it is completed
                            attachObserverToReviewOfUser(userData: userData, userId: userId, reviewId: reviewId)
                        }
                    }
                    else {
                        // Review is already available locally, so it's contents are either
                        // already being listened to or are guaranteed to be final
                    }
                    
                }
            }
            else { // User is not admin so store things in current user only
                print("*** Executing non-admin branch of attachObserverToReviewIdsOfUser ***")
                for (reviewId, isComplete) in reviewIds {
                    
                    // Initialize userId local reviews dictionary
                    if userData.currentUserReviews[reviewId] == nil {

                        // This review is new to local so it should either be gotten or observed
                        if isComplete { // Get once, dont need to observe
                            getReviewOfUser(userData: userData, userId: userId, reviewId: reviewId)
                        }
                        else { // Observe since we need to know when it is completed
                            attachObserverToReviewOfUser(userData: userData, userId: userId, reviewId: reviewId)
                        }
                    }
                    else {
                        // Review is already available locally, so it's contents are either
                        // already being listened to or are guaranteed to be final
                    }
                    
                }
            }
        }
        else {
            
            if let userReviews = userData.allReviews[userId] {
                if !userReviews.isEmpty {
                    userData.allReviews[userId] = [String : LessonReview]()
                }
            }

            
            print("*** Could not get review_ids as dictionary ***")
        }
    })
}

func getReviewOfUser(userData: UserData, userId: String, reviewId: String) {
    database.child("reviews").child(userId).child(reviewId).observeSingleEvent(of: .value, with: {(snapshot) in
        
        if let reviewDict = snapshot.value as? [String: Any] { // Dictionary of all user's reviews
            // Attempt to read single user from allUsersDict
            
            let lessonReview = getLessonReviewFromReviewDict(reviewDict: reviewDict)
            
            // Place the review in the right spot
            userData.allReviews[userId]![reviewId] = lessonReview
        }
        else {
            print("*** Could not read user review as dictionary ***")
        }
    })
}

func attachObserverToReviewOfUser(userData: UserData, userId: String, reviewId: String) {
    
    reviewHandles[reviewId] = database.child("reviews").child(userId).child(reviewId).observe(DataEventType.value, with: {(snapshot) in
        
        if let reviewDict = snapshot.value as? [String: Any] { // Dictionary of all user's reviews
            // Attempt to read single user from allUsersDict
            
            let lessonReview = getLessonReviewFromReviewDict(reviewDict: reviewDict)
            
            if lessonReview.isComplete {
                database.removeObserver(withHandle: reviewHandles[reviewId]!)
            }
            // Place the review in the right spot
            
            if userData.currentUser.isAdmin {
                userData.allReviews[userId]![reviewId] = lessonReview
            }
            else { // User is not review so store it in current user reviews
                userData.currentUserReviews[reviewId] = lessonReview
            }
        }
        else {
            print("*** Could not read user review as dictionary ***")
        }
    })
}


/**
 CLIENT SIDE FUNCTIONS
 */

func clientLaunch(userData: UserData) {
    database.child("review_ids").child(userData.currentUser.id).observe(DataEventType.value, with: {(snapshot) in
        
        if let reviewIds = snapshot.value as? [String: Bool] {
            
            for (reviewId, isComplete) in reviewIds {
                
                for (reviewKey, _) in userData.currentUserReviews {
                    if reviewIds[reviewKey] == nil {
                        userData.currentUserReviews.removeValue(forKey: reviewKey)
                        reviewHandles.removeValue(forKey: reviewKey)
                    }
                }
                
                // Initialize userId local reviews dictionary
                if userData.currentUserReviews[reviewId] == nil {

                    // This review is new to local so it should either be gotten or observed
                    if isComplete { // Get once, dont need to observe
                        getReviewOfCurrentUser(userData: userData, reviewId: reviewId)
                    }
                    else { // Observe since we need to know when it is completed
                        attachObserverToReviewOfCurrentUser(userData: userData, reviewId: reviewId)
                    }
                }
                else {
                    // Review is already available locally, so it's contents are either
                    // already being listened to or are guaranteed to be final
                }
            } // End of for loop
        }
        else {
            
            if !userData.currentUserReviews.isEmpty {
                userData.currentUserReviews = [String : LessonReview]()
            }
            
            print("*** Could not get review_ids as dictionary ***")
        }
    })
}

func getReviewOfCurrentUser(userData: UserData, reviewId: String) {
    database.child("reviews").child(userData.currentUser.id).child(reviewId).observeSingleEvent(of: .value, with: {(snapshot) in
        
        if let reviewDict = snapshot.value as? [String: Any] { // Dictionary of all user's reviews
            // Attempt to read single user from allUsersDict
            
            let lessonReview = getLessonReviewFromReviewDict(reviewDict: reviewDict)
            
            // Place the review in currentUserReviews
            userData.currentUserReviews[reviewId] = lessonReview
        }
        else {
            print("*** Could not read user review as dictionary ***")
        }
    })
}

func attachObserverToReviewOfCurrentUser(userData: UserData, reviewId: String) {
    
    reviewHandles[reviewId] = database.child("reviews").child(userData.currentUser.id).child(reviewId).observe(DataEventType.value, with: {(snapshot) in
        
        if let reviewDict = snapshot.value as? [String: Any] { // Dictionary of all user's reviews
            // Attempt to read single user from allUsersDict
            print("*** Completion Block Called ***")
            
            let lessonReview = getLessonReviewFromReviewDict(reviewDict: reviewDict)
            
            // If the review has been completed, remove observer.
            if lessonReview.isComplete && reviewHandles[reviewId] != nil {
                database.removeObserver(withHandle: reviewHandles[reviewId]!)
                reviewHandles.removeValue(forKey: reviewId)
            }
            // Place the review in the right spot
            userData.currentUserReviews[reviewId] = lessonReview
        }
        else {
            print("*** Could not read user review as dictionary ***")
        }
    })
    
}

/**
 FUNCTIONS TO PARSE DICTIONARIES
 */

func getLessonReviewFromReviewDict(reviewDict: [String: Any]) -> LessonReview {
    var lessonReview = LessonReview(
        id: reviewDict["id"] as! String,
        lessonDate: reviewDict["lessonDate"] as! String,
        submittedBy: reviewDict["submittedBy"] as! String,
        isComplete: true
    )
    
    if let adminReviewDict = reviewDict["admin"] as? [String: Any] {
        lessonReview.admin = getAdminReviewFromDict(adminReviewDict: adminReviewDict)
    }
    else {
        lessonReview.isComplete = false
    }
    
    if let clientReviewDict = reviewDict["client"] as? [String: Any] {
        lessonReview.client = getClientReviewFromDict(clientReviewDict: clientReviewDict)
    }
    else {
        lessonReview.isComplete = false
    }
    
    return lessonReview
}

func getAdminReviewFromDict(adminReviewDict: [String: Any]) -> AdminReview {
    
    var newAdminReview = AdminReview()
    newAdminReview.forehandCurr = adminReviewDict["forehandCurr"] as! Bool
    newAdminReview.forehandNext = adminReviewDict["forehandNext"] as! Bool
    
    newAdminReview.backhandCurr = adminReviewDict["backhandCurr"] as! Bool
    newAdminReview.backhandNext = adminReviewDict["backhandNext"] as! Bool
    
    newAdminReview.forehandVolleyCurr = adminReviewDict["forehandVolleyCurr"] as! Bool
    newAdminReview.forehandVolleyNext = adminReviewDict["forehandVolleyNext"] as! Bool
    
    newAdminReview.backhandVolleyCurr = adminReviewDict["backhandVolleyCurr"] as! Bool
    newAdminReview.backhandVolleyNext = adminReviewDict["backhandVolleyNext"] as! Bool
    
    newAdminReview.overhandCurr = adminReviewDict["overhandCurr"] as! Bool
    newAdminReview.overhandNext = adminReviewDict["overhandNext"] as! Bool
    
    newAdminReview.reverseForehandCurr = adminReviewDict["reverseForehandCurr"] as! Bool
    newAdminReview.reverseForehandNext = adminReviewDict["reverseForehandNext"] as! Bool
    
    newAdminReview.dropshotCurr = adminReviewDict["dropshotCurr"] as! Bool
    newAdminReview.dropshotNext = adminReviewDict["dropshotNext"] as! Bool
    
    newAdminReview.lobCurr = adminReviewDict["lobCurr"] as! Bool
    newAdminReview.lobNext = adminReviewDict["lobNext"] as! Bool
    
    newAdminReview.serveCurr = adminReviewDict["serveCurr"] as! Bool
    newAdminReview.serveNext = adminReviewDict["serveNext"] as! Bool
    
    newAdminReview.returnServeCurr = adminReviewDict["returnServeCurr"] as! Bool
    newAdminReview.returnServeNext = adminReviewDict["returnServeNext"] as! Bool
    
    newAdminReview.quicknessCurr = adminReviewDict["quicknessCurr"] as! Bool
    newAdminReview.quicknessNext = adminReviewDict["quicknessNext"] as! Bool
    
    newAdminReview.agilityCurr = adminReviewDict["agilityCurr"] as! Bool
    newAdminReview.agilityNext = adminReviewDict["agilityNext"] as! Bool
    
    newAdminReview.balanceCurr = adminReviewDict["balanceCurr"] as! Bool
    newAdminReview.balanceNext = adminReviewDict["balanceNext"] as! Bool
    
    newAdminReview.reactionTimeCurr = adminReviewDict["reactionTimeCurr"] as! Bool
    newAdminReview.reactionTimeNext = adminReviewDict["reactionTimeNext"] as! Bool
    
    newAdminReview.aerobicCurr = adminReviewDict["aerobicCurr"] as! Bool
    newAdminReview.aerobicNext = adminReviewDict["aerobicNext"] as! Bool
    
    newAdminReview.strategyCurr = adminReviewDict["strategyCurr"] as! Bool
    newAdminReview.strategyNext = adminReviewDict["strategyNext"] as! Bool
    
    newAdminReview.mentalToughnessCurr = adminReviewDict["mentalToughnessCurr"] as! Bool
    newAdminReview.mentalToughnessNext = adminReviewDict["mentalToughnessNext"] as! Bool
    
    newAdminReview.notes = adminReviewDict["notes"] as! String
    
    return newAdminReview
    
}

func getClientReviewFromDict(clientReviewDict: [String: Any]) -> ClientReview {
    
    var newClientReview =  ClientReview()
    newClientReview.head = clientReviewDict["head"] as! Double
    newClientReview.heart = clientReviewDict["heart"] as! Double
    newClientReview.lungs = clientReviewDict["lungs"] as! Double
    newClientReview.legs = clientReviewDict["legs"] as! Double
    newClientReview.funMeter = clientReviewDict["funMeter"] as! Double
    newClientReview.notes = clientReviewDict["notes"] as! String
    
    return newClientReview
}

func getDbUserFromUserDict(userDict: [String: Any]) -> DbUser {
    var newUser = DbUser(isAdmin: userDict["isAdmin"] as! Bool)
    
    newUser.id = userDict["id"] as! String
    newUser.firstName = userDict["firstName"] as! String
    newUser.lastName = userDict["lastName"] as! String
    newUser.birthday = userDict["birthday"] as! String
    newUser.phone = userDict["phone"] as! String
    newUser.email = userDict["email"] as! String
    newUser.ustaNumber = userDict["ustaNumber"] as! String
    newUser.utrNumber = userDict["utrNumber"] as! String
    
    return newUser
    
}

/**
 DELETION FUNCTIONS
 */

func deleteClientReviewOfCurrentUser(userData: UserData, reviewId: String) {
    
    database.child("review_ids").child(userData.currentUser.id).child(reviewId).setValue(false) { (error:Error?, ref:DatabaseReference) in
        
        if let error = error {
            print("*** Review_id could not be set to false: \(error.localizedDescription)")

        }
        else {
            print("*** Review_id was set to false. Attempting to delete review itself ***")
            
            database.child("reviews").child(userData.currentUser.id).child(reviewId).child("client").removeValue { (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    print("*** Client review could not be deleted: \(error.localizedDescription)")

                }
                else {
                    print("*** Client review was deleted. Attempting to delete review itself ***")
                }
            }
        }
    }
}

func deleteAdminReviewOfUserWithId(reviewId: String, userId: String) {
    
    database.child("review_ids").child(userId).child(reviewId).setValue(false) { (error:Error?, ref:DatabaseReference) in
        
        if let error = error {
            print("*** Review_id could not be set to false: \(error.localizedDescription)")

        }
        else {
            print("*** Review_id was set to false. Attempting to delete review itself ***")
            
            database.child("reviews").child(userId).child(reviewId).child("admin").removeValue { (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    print("*** Admin review could not be deleted: \(error.localizedDescription)")

                }
                else {
                    print("*** Admin review was deleted. Attempting to delete review itself ***")
                }
            }
        }
    }
}

func deleteLessonReviewOfUserWithId(reviewId: String, userId: String) {
    database.child("review_ids").child(userId).child(reviewId).removeValue { (error:Error?, ref:DatabaseReference) in
        
        if let error = error {
            print("*** Review_id could not be deleted: \(error.localizedDescription)")

        }
        else {
            print("*** REVIEW_ID WAS DELETED ***")
            
            database.child("reviews").child(userId).child(reviewId).removeValue { (error:Error?, ref:DatabaseReference) in
                if let error = error {
                    print("*** Lesson review could not be deleted: \(error.localizedDescription)")

                }
                else {
                    print("*** Lesson review was deleted ***")
                    reviewHandles.removeValue(forKey: reviewId)
                }
            }
        }
    }
}

func deleteCurrentUserAccount(userData: UserData) {
    let requestGroup = DispatchGroup()
    
    requestGroup.enter()
    database.child("user_ids").child(userData.currentUser.id).removeValue { (error:Error?, ref:DatabaseReference) in
        
        if let error = error {
            print("*** user_id could not be deleted: \(error.localizedDescription)")
        }
        else {
            print("*** user_id was deleted")
            
            requestGroup.enter()
            requestGroup.enter()
            requestGroup.enter()
            requestGroup.enter()

            database.child("reviews").child(userData.currentUser.id).removeValue { (error:Error?, ref:DatabaseReference) in
                
                if let error = error {
                    print("*** reviews could not be deleted: \(error.localizedDescription)")
                }
                else {
                    print("*** reviews were deleted")
                    
                    
                }
                requestGroup.leave()
            }
            database.child("review_ids").child(userData.currentUser.id).removeValue { (error:Error?, ref:DatabaseReference) in
                
                if let error = error {
                    print("*** review_ids could not be deleted: \(error.localizedDescription)")
                }
                else {
                    print("*** review_ids were deleted")
                    
                    
                }
                requestGroup.leave()
            }
            
            database.child("users").child(userData.currentUser.id).removeValue { (error:Error?, ref:DatabaseReference) in
                
                if let error = error {
                    print("*** users could not be deleted: \(error.localizedDescription)")
                }
                else {
                    print("*** users was deleted")
                    
                    
                }
                requestGroup.leave()
            }
            
            // Delete user's profile image if it exists
            
            storage.child("images").child("\(userData.currentUser.id).jpg").delete { error in
                
                if let error = error {
                    // Image could not be deleted
                    print("Image could not be delted: \(error.localizedDescription)")
                }
                else {
                    print("User image was deleted")
                }
                
                requestGroup.leave()
            }
        }
        requestGroup.leave()
    }
    
    requestGroup.notify(queue: .main) {
        let user = Auth.auth().currentUser
        
        user?.delete { error in
            if let error = error {
                // Could not delete account
                print(error.localizedDescription)
            }
            else {
                // Account deleted
                print("Account successfully deleted")
            }
        }
        
        logOutCurrentUser(userData: userData)
    }
}

/**
 DESTRUCTOR FUNCTIONS
 */

/*
 Logs out the currently authorized user and detaches all observers
 */
func logOutCurrentUser(userData: UserData) {
    do {
        try Auth.auth().signOut()
    }
    catch let signOutError as NSError {
        print("Error signing ot: %@", signOutError)
    }
    
    purgeLocalUserData(userData: userData)
    userData.userAuthenticated = false
    
    // Clear all observer handles
    detachAllObservers()
    
}

/*
 Detaches all of the client's current observers
 */
func detachAllObservers() {
    for (_, value) in userHandles {
        database.removeObserver(withHandle: value)
    }
    
    for (_, value) in reviewHandles {
        database.removeObserver(withHandle: value)
    }
    
    for (_, value) in reviewIdHandles {
        database.removeObserver(withHandle: value)
    }
    
    userHandles = [String: UInt]()
    reviewHandles = [String: UInt]()
    reviewIdHandles = [String: UInt]()

}

func purgeLocalUserData(userData: UserData) {
    userData.allReviews = [String : [String: LessonReview]]()
    userData.allUsers = [String: DbUser]()
    userData.currentUserReviews = [String: LessonReview]()
}
