//
//  UtilityFunctions.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI

/*
**********************************
MARK: - Get Image from Binary Data
**********************************
*/
public func getImageFromBinaryData(binaryData: Data?, defaultSystemName: String) -> Image {
  
    // Create a UIImage object from binaryData
    let uiImage = UIImage(data: binaryData!)
  
    // Unwrap uiImage to see if it has a value
    if let imageObtained = uiImage {
      
        // Image is successfully obtained
        return Image(uiImage: imageObtained)
      
    } else {
        /*
         Image file with name 'defaultFilename' is returned if the image cannot be obtained
         from the binaryData given. Image file 'defaultFilename' must be given in Assets.xcassets
         */
        return Image(systemName: defaultSystemName)
    }
}
