//
//  ChangeSetting.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI
import FirebaseAuth
import FirebaseDatabase

struct ChangeSetting: View {
    let attribute: String
    
    @EnvironmentObject var userData: UserData

    
    @State private var enteredField = ""
    
    @State private var reAuthEmail = ""
    @State private var reAuthPassword = ""

    @State private var newBirthday = Date()
    @State private var showInputDataAlert = false
    @State private var showConfirmSaveAlert = false
    
    @State private var alertTitle = ""
    @State private var alertDescription = ""
    @State private var dataSavedSuccess = false
    


    @Environment(\.presentationMode) var presentationMode

    
    var body: some View {
        ZStack {
            VStack {
                
                if (attribute == "Birthday") { // Show datepicker
                    DatePicker("", selection: $newBirthday, in: ...Date(), displayedComponents: .date)
                    .datePickerStyle(GraphicalDatePickerStyle())
                        .padding()
                        .alert(isPresented: $showInputDataAlert, content: { self.inputDataAlert })

                }
                else if (attribute == "Password" || attribute == "Email") {
                    HStack {
                        Spacer()
                        TextField("Enter Account Email", text: $reAuthEmail)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .frame(width: 300, height: 36)
                        

                        // Button to clear the text field
                        Button(action: {
                            self.reAuthEmail = ""
                        }) {
                            Image(systemName: "clear")
                                .imageScale(.medium)
                                .font(Font.title.weight(.regular))
                                .foregroundColor(.accentColor)
                                .shadow(radius: 5)                    }
                        Spacer()
                    }
                    .padding(.bottom, 10)

                    HStack {
                        Spacer()
                        SecureField("Enter Current Password", text: $reAuthPassword)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .frame(width: 300, height: 36)


                        // Button to clear the text field
                        Button(action: {
                            self.reAuthPassword = ""
                        }) {
                            Image(systemName: "clear")
                                .imageScale(.medium)
                                .font(Font.title.weight(.regular))
                                .foregroundColor(.accentColor)
                                .shadow(radius: 5)                    }
                        Spacer()
                    }
                    .padding(.bottom, 10)
                    HStack {
                        Spacer()
                        if (attribute == "Email") {
                            TextField("Enter New \(attribute)", text: $enteredField)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                                .frame(width: 300, height: 36)

                        }
                        else {
                            SecureField("Enter New \(attribute)", text: $enteredField)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                                .frame(width: 300, height: 36)
                        }
                        

                        // Button to clear the text field
                        Button(action: {
                            self.enteredField = ""
                        }) {
                            Image(systemName: "clear")
                                .imageScale(.medium)
                                .font(Font.title.weight(.regular))
                                .foregroundColor(.accentColor)
                                .shadow(radius: 5)
                        }
                        Spacer()
                    }
                    .alert(isPresented: $showInputDataAlert, content: { self.inputDataAlert })
                    .padding(.bottom, 10)

                }
                else {
                    HStack {
                        Spacer()
                        TextField("Enter New \(self.attribute)", text: $enteredField)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .frame(width: 300, height: 36)
                       
                        // Button to clear the text field
                        Button(action: {
                            self.enteredField = ""
                        }) {
                            Image(systemName: "clear")
                                .imageScale(.medium)
                                .font(Font.title.weight(.regular))
                                .foregroundColor(.accentColor)
                                .shadow(radius: 5)
                        }
                        Spacer()
                    }
                    .alert(isPresented: $showInputDataAlert, content: { self.inputDataAlert })
                    .padding(.bottom, 10)
                    
                }
                
                Button(action: {
                    if inputDataValidated() {
                        print("Data successfully validated")
                        self.showConfirmSaveAlert = true
                    }
                    else {
                        print("Data failed to validate")
                        self.showInputDataAlert = true
                    }
                }
                ) {
                    Text("Save")
                        .font(Font.subheadline.weight(.semibold))
                        .frame(width: 100, height: 36, alignment: .center)
                        .foregroundColor(.white)
                        .background(
                            RoundedRectangle(cornerRadius: 16)
                                .foregroundColor(.accentColor)
                                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                        )
                        .alert(isPresented: $showConfirmSaveAlert, content: { self.confirmSaveAlert })

                        
                }

            } // End of VStack
            .font(.system(size: 14))
            .navigationBarTitle("\(attribute)")
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            .background(backgroundGradient).ignoresSafeArea(.all)

        }

        
    }
    
    
    
    var inputDataAlert: Alert {
        Alert(title: Text(alertTitle),
              message: Text(alertDescription),
              dismissButton: .default(Text("OK")){
                if dataSavedSuccess {
                    self.presentationMode.wrappedValue.dismiss()
                }
              })
    }
    
    var confirmSaveAlert: Alert {
        Alert(title: Text("Confirm"),
              message: Text("Do you wish to save changes?"),
              primaryButton: .default(Text("OK")) {
                updateAttribute()
              },
              secondaryButton: .default(Text("Cancel")) {
                // Cancel operation. Basically do nothing
              })
    }
    
    func updateAttribute() {
        switch self.attribute {
        case "USTA ID":
            updateUsta()
        case "UTR":
            updateUtr()
        case "Email":
            updateEmail()
        case "Phone Number":
            updatePhone()
        case "Password":
            updatePassword()
        case "Birthday":
            updateBirthday()
        default:
            print("This attribute is not supported")
        }
    }
    
    func inputDataValidated() -> Bool {
        
        switch self.attribute {
        case "USTA ID":
            
            if self.enteredField.isEmpty {
                alertTitle = "Missing Data"
                alertDescription = "Make sure you've entered all necessary fields"
                return false
            }
            else if (!CharacterSet(charactersIn: enteredField).isSubset(of: CharacterSet.decimalDigits)) {
                alertTitle = "Invalid USTA ID"
                alertDescription = "Please make sure your USTA ID contains only numbers"
                return false
            }
        
        case "UTR":
            
            if self.enteredField.isEmpty {
                alertTitle = "Missing Data"
                alertDescription = "Make sure you've entered all necessary fields"
                return false
            }
            else if (!CharacterSet(charactersIn: enteredField).isSubset(of: CharacterSet(charactersIn: "0123456789.")) || !isValidUTR(utr: enteredField)) {
                alertTitle = "Invalid UTR"
                alertDescription = "Please make sure your Universal Tennis Rating contains only a decimal value between 1.00 and 16.50"
                return false
            }
            
        case "Email":
            if self.enteredField.isEmpty || self.reAuthEmail.isEmpty || self.reAuthPassword.isEmpty {
                alertTitle = "Missing Data"
                alertDescription = "Make sure you've entered all necessary fields"
                return false
            }
        case "Phone Number":
            if self.enteredField.isEmpty {
                alertTitle = "Missing Data"
                alertDescription = "Make sure you've entered all necessary fields"
                return false
            }
            else if (!CharacterSet(charactersIn: enteredField).isSubset(of: CharacterSet.decimalDigits.union(CharacterSet(charactersIn: " ")))
                     || formatPhone(phoneNumber: enteredField) == nil
            ) {
                alertTitle = "Invalid Phone Number"
                alertDescription = "Please make sure your phone number contains only numbers and spaces and is at most 11 digits"
                return false
            }
        case "Password":
            if self.enteredField.isEmpty {
                alertTitle = "Missing Data"
                alertDescription = "Make sure you've entered all necessary fields"
                return false
            }
            
        case "Birthday":
            return true
            
        default:
            print("This attribute is not supported")
        }
        
        return true
    }
    
    func updateUsta() {
        database.child("users").child(userData.currentUser.id).child("ustaNumber").setValue(enteredField) { (error:Error?, ref:DatabaseReference) in
            
            if let error = error {
                print("USTA could not be set: \(error.localizedDescription)")
                alertTitle = "Error"
                alertDescription = "USTA could not be set"
                showInputDataAlert = true
            }
            else {
                print("USTA was successfully set")
                alertTitle = "Success"
                alertDescription = "Your USTA ID was successfully changed"
                dataSavedSuccess = true
                showInputDataAlert = true
            }
        }
    }
    
    func updateUtr() {
        database.child("users").child(userData.currentUser.id).child("utrNumber").setValue(roundStringNumToTwoPlaces(utr: enteredField)) { (error:Error?, ref:DatabaseReference) in
            
            if let error = error {
                print("UTR could not be set: \(error.localizedDescription)")
                alertTitle = "Error"
                alertDescription = "UTR could not be set"
                showInputDataAlert = true
            }
            else {
                print("UTR was successfully set")
                alertTitle = "Success"
                alertDescription = "Your UTR was successfully changed"
                dataSavedSuccess = true
                showInputDataAlert = true
            }
        }
        
    }
    
    func updateEmail() {
        if let user = Auth.auth().currentUser {
            let credential = EmailAuthProvider.credential(withEmail: self.reAuthEmail, password: self.reAuthPassword)
            
            user.reauthenticate(with: credential) { (authResult:AuthDataResult?, error:Error?) in
                
                if let error = error as NSError? {
                    print("Could not reauthenticate user: \(error.localizedDescription)")
                    switch AuthErrorCode(rawValue: error.code) {
                    case .userDisabled:
                        alertTitle = "Error"
                        alertDescription = "The user account matching the provided credentials has been disabled. Contact administrators if this is a mistake"
                    case .operationNotAllowed:
                        alertTitle = "Error"
                        alertDescription = "Email and password accounts are not supported"
                    case .invalidCredential:
                        alertTitle = "Error"
                        alertDescription = "The credential provided was malformed or has expired"
                    case .emailAlreadyInUse:
                        alertTitle = "Error"
                        alertDescription = "The new email provided is already in use"
                    case .wrongPassword:
                        alertTitle = "Error"
                        alertDescription = "Incorrect password or email, please check your credentials"
                    case .invalidEmail:
                        alertTitle = "Error"
                        alertDescription = "Your email was invalid or otherwise malformed"
                    default:
                        alertTitle = "Error"
                        alertDescription = "Email could not be set. Please check that the provided credentials match the currently signed in user"
                    }
                    showInputDataAlert = true
                }
                else {
                    print("User was reauthenticated. Now attempt to reset email")
                    
                    if let reAuthUser = Auth.auth().currentUser {
                        
                        reAuthUser.updateEmail(to: self.enteredField) { (error:Error?) in
                            if let error = error as NSError? {
                                print("Email could not be set: \(error.localizedDescription)")
                                
                                switch AuthErrorCode(rawValue: error.code) {
                                
                                case .emailAlreadyInUse:
                                    alertTitle = "Error"
                                    alertDescription = "The new email provided is already in use"
                                case .requiresRecentLogin:
                                    alertTitle = "Error"
                                    alertDescription = "This operation requires a recent login"
                                case .invalidEmail:
                                    alertTitle = "Error"
                                    alertDescription = "Your email was invalid or otherwise malformed"
                                default:
                                    alertTitle = "Error"
                                    alertDescription = "Email could not be set"
                                }
                                showInputDataAlert = true

                            }
                            else {
                                
                                database.child("users").child(userData.currentUser.id).child("email").setValue(enteredField) { (error:Error?, ref:DatabaseReference) in
                                    
                                    if let error = error {
                                        print("Email could not be set: \(error.localizedDescription)")
                                        alertTitle = "Error"
                                        alertDescription = "Email could not be set"
                                        showInputDataAlert = true
                                    }
                                    else {
                                        print("Email was successfully set")
                                        alertTitle = "Success"
                                        alertDescription = "Your email was successfully changed"
                                        dataSavedSuccess = true
                                        showInputDataAlert = true
                                    }
                                }
                                
                            }
                        }
                    }
                    else {
                        print("Could not get user after reauthentication")
                        alertTitle = "Error"
                        alertDescription = "There was an issue authenticating you. Please try again or restart the app. "
                        showInputDataAlert = true

                    }
                }
            }
        }
        else {
            print("Could not get currently authorized user")
        }
    }
    
    func updatePhone() {
        database.child("users").child(userData.currentUser.id).child("phone").setValue(enteredField) { (error:Error?, ref:DatabaseReference) in
            
            if let error = error {
                print("Phone number could not be set: \(error.localizedDescription)")
                alertTitle = "Error"
                alertDescription = "Phone number could not be set"
                showInputDataAlert = true
            }
            else {
                print("Phone number was successfully set")
                alertTitle = "Success"
                alertDescription = "Your phone number was successfully changed"
                dataSavedSuccess = true
                showInputDataAlert = true
            }
        }
    }
    
    func updateBirthday() {
        let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .long
            return formatter
        }()
        
        let birthDateString = dateFormatter.string(from: newBirthday)
        database.child("users").child(userData.currentUser.id).child("birthday").setValue(birthDateString) { (error:Error?, ref:DatabaseReference) in
            
            if let error = error {
                print("Birthday could not be set: \(error.localizedDescription)")
                alertTitle = "Error"
                alertDescription = "Birthday could not be set"
                showInputDataAlert = true
            }
            else {
                print("Birthday was successfully set")
                alertTitle = "Success"
                alertDescription = "Your birthday was successfully changed"
                dataSavedSuccess = true
                showInputDataAlert = true
            }
        }
    }
    
    func updatePassword() {
        
        if let user = Auth.auth().currentUser {
            let credential = EmailAuthProvider.credential(withEmail: self.reAuthEmail, password: self.reAuthPassword)
            
            user.reauthenticate(with: credential) { (authResult:AuthDataResult?, error:Error?) in
                
                if let error = error as NSError?{
                    print("Could not reauthenticate user: \(error.localizedDescription)")
                    switch AuthErrorCode(rawValue: error.code) {
                    case .userDisabled:
                        alertTitle = "Error"
                        alertDescription = "The user account matching the provided credentials has been disabled. Contact administrators if this is a mistake"
                    case .operationNotAllowed:
                        alertTitle = "Error"
                        alertDescription = "Email and password accounts are not supported"
                    case .invalidCredential:
                        alertTitle = "Error"
                        alertDescription = "The credential provided was malformed or has expired"
                    case .emailAlreadyInUse:
                        alertTitle = "Error"
                        alertDescription = "The new email provided is already in use"
                    case .wrongPassword:
                        alertTitle = "Error"
                        alertDescription = "Incorrect password or email, please check your credentials"
                    case .invalidEmail:
                        alertTitle = "Error"
                        alertDescription = "Your email was invalid or otherwise malformed"
                    default:
                        alertTitle = "Error"
                        alertDescription = "Email could not be set"
                    }
                    showInputDataAlert = true
                }
                else {
                    print("User was reauthenticated. Now attempt to reset password")
                    
                    if let reAuthUser = Auth.auth().currentUser {
                        
                        reAuthUser.updatePassword(to: self.enteredField) { (error:Error?) in
                            if let error = error as NSError? {
                                print("Password could not be set: \(error.localizedDescription)")
                                
                                switch AuthErrorCode(rawValue: error.code) {
                                case .weakPassword:
                                    alertTitle = "Error"
                                    alertDescription = "The provided new password is too weak. \(error.userInfo[NSLocalizedFailureReasonErrorKey] ?? "Strenghten your password by adding length, symbols, and variable casing")"
                                case .requiresRecentLogin:
                                    alertTitle = "Error"
                                    alertDescription = "This operation requires a recent login"
                                case .operationNotAllowed:
                                    alertTitle = "Error"
                                    alertDescription = "Sign in with passwords is disabled"
                                default:
                                    alertTitle = "Error"
                                    alertDescription = "Your password was successfully changed"
                                }
                                showInputDataAlert = true
                            }
                            else {
                                print("Password was successfully set")
                                alertTitle = "Success"
                                alertDescription = "Your password was successfully changed"
                                dataSavedSuccess = true
                                showInputDataAlert = true
                            }
                        }
                    }
                    else {
                        print("Could not get user after reauthentication")
                        alertTitle = "Error"
                        alertDescription = "There was an issue authenticating you. Please try again or restart the app. "
                        showInputDataAlert = true
                    }
                }
            }
        }
        else {
            print("Could not get currently authorized user")
        }
    }
    
    
    func roundStringNumToTwoPlaces(utr: String) -> String? {
        
        if let doubleUtr = Double(utr) {
           return String(format: "%.2f", doubleUtr)
        }
        else {
            return nil
        }
       
    }
    
    func isValidUTR(utr: String) -> Bool {
        if let doubleUtr = Double(utr) {
            return (1.00 <= doubleUtr) && (doubleUtr <= 16.50)
        }
        else {
            return false
        }
    }
    
}
