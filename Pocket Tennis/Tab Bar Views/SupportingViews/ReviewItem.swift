//
//  ReviewItem.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/20/21.
//

import SwiftUI

struct ReviewItem: View {
    let user: DbUser
    let lessonReview: LessonReview
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        HStack {
            
            VStack(alignment: .leading) {
                Text("Lesson on:")
                    .font(.system(size: 13))
                    .foregroundColor(.white)
                    .opacity(0.6)
                Text(lessonReview.lessonDate)
                    .font(.system(size: 25))
                    .foregroundColor(.accentColor)
                Text("with \(userData.currentUser.isAdmin ? (self.user.firstName + " " + self.user.lastName) : lessonReview.submittedBy)")
                    .font(.system(size: 13))
                    .foregroundColor(.white)
                    .opacity(0.6)
            }
            
            Spacer()
            VStack {
                if !lessonReview.isComplete {
                    Image(systemName: "exclamationmark.circle")
                        .imageScale(.medium)
                        .foregroundColor(.accentColor)
                        .font(Font.title2)
                    
                }
            }

        }
    }
}
