//
//  Clients-Admin.swift
//  Racket
//
//  Created by Harlan  Reese Pounders on 3/16/21.
//

import SwiftUI

struct ClientsList: View {
    
    @EnvironmentObject var userData: UserData
    @State private var searchItem = ""
    
    var body: some View {
        NavigationView {
                
            if userData.allUsers.isEmpty {
                VStack {
                    HStack {
                        Text("There are currently no clients to list")
                            .multilineTextAlignment(.center)
                            .frame(width: 200, alignment: .center)
                            .font(Font.subheadline.weight(.semibold))
                            .padding(10)
                            .opacity(0.6)
                    } // Hstack
                } // Vstack
                .navigationBarTitle(Text("Clients"))

                
            }
            else {
                ZStack {
                    backgroundGradient
                        .edgesIgnoringSafeArea(.all)
                    VStack {
                        SearchBar(placeholder: "Search Clients", text: self.$searchItem)
                        
                        List {
                            ForEach(Array(userData.allUsers.values).filter { aUser in
                                return searchItem.isEmpty ||
                                    aUser.firstName.localizedStandardContains(searchItem) ||
                                    aUser.lastName.localizedStandardContains(searchItem) ||
                                    aUser.ustaNumber.localizedStandardContains(searchItem) ||
                                    aUser.email.localizedStandardContains(searchItem)
                            }.sorted(by: { (userA, userB) -> Bool in
                                return (userA.firstName + userA.lastName) < (userB.firstName + userB.lastName)
                            }), id: \.id) { user in
                                NavigationLink(destination: ClientDetails(user: user)) {
                                    ClientItem(user: user)
                                }
                            }
                        }
                        .listStyle(InsetGroupedListStyle())
                    } // Vstack
                    
                } // ZStack
                .navigationBarTitle(Text("Clients"))
  
            }

            
        } // End of NavigationView
        .font(.system(size: 14))
        .navigationViewStyle(StackNavigationViewStyle())

    } // End of body
    
}
