//
//  LessonReviewPart2.swift
//  Racket
//
//  Created by Harlan  Reese Pounders on 3/17/21.
//

import SwiftUI
import FirebaseDatabase

struct LessonReviewAdminForm: View {
    let user: DbUser
    @EnvironmentObject var userData: UserData
    @Environment(\.presentationMode) var presentationMode


    @State private var date = Date()
    @State private var adminReview = AdminReview()
    
    @State private var formAlertTitle = ""
    @State private var formAlertDescription = ""
    @State private var showFormAlert = false

    
    @State private var columns: [GridItem] = [
        GridItem(.fixed(160), spacing: 0, alignment: .leading),
        GridItem(.fixed(80), spacing: 0, alignment: .center),
        GridItem(.fixed(70), spacing: 0, alignment: .center)
    ]
    
    var dateClosedRange: ClosedRange<Date> {
        // Set minimum date to 2 weeks earlier than the current year
        let minDate = Calendar.current.date(byAdding: .day, value: -14, to: Date())!
       
        return minDate...Date()
    }
        
    var body: some View {
        Form {
            Section(header: Text("Lesson Date")){
                DatePicker("", selection: $date, in: dateClosedRange, displayedComponents: [.date])
                    .datePickerStyle(GraphicalDatePickerStyle())
            }
            Section(header: Text("Strokes")) {
                LazyVGrid(columns: columns, spacing: 25) {
                    Section() {
                        Text("")
                        Text("Current")
                        Text("Next")
                    }
                    Section() {
                        Text("Forehand")
                        Toggle("", isOn: $adminReview.forehandCurr)
                        Toggle("", isOn: $adminReview.forehandNext)
                        Text("Backhand")
                        Toggle("", isOn: $adminReview.backhandCurr)
                        Toggle("", isOn: $adminReview.backhandNext)
                        Text("Forehand Volley")
                        Toggle("", isOn: $adminReview.forehandVolleyCurr)
                        Toggle("", isOn: $adminReview.forehandVolleyNext)
                    }
                    Section() {
                        Text("Backhand Volley")
                        Toggle("", isOn: $adminReview.backhandVolleyCurr)
                        Toggle("", isOn: $adminReview.backhandVolleyNext)
                        Text("Reverse Forehand")
                        Toggle("", isOn: $adminReview.reverseForehandCurr)
                        Toggle("", isOn: $adminReview.reverseForehandNext)
                        Text("Dropshot")
                        Toggle("", isOn: $adminReview.dropshotCurr)
                        Toggle("", isOn: $adminReview.dropshotNext)
                    }
                    Section() {
                        Text("Lob")
                        Toggle("", isOn: $adminReview.lobCurr)
                        Toggle("", isOn: $adminReview.lobNext)
                        Text("Serve")
                        Toggle("", isOn: $adminReview.serveCurr)
                        Toggle("", isOn: $adminReview.serveNext)
                        Text("Return")
                        Toggle("", isOn: $adminReview.returnServeCurr)
                        Toggle("", isOn: $adminReview.returnServeNext)
                    }
                }
                .font(.system(size: 20))
                .padding(.top, 15)
                .padding(.bottom, 15)
            }
            Section(header: Text("Skills")) {
                LazyVGrid(columns: columns, spacing: 25) {
                    Section() {
                        Text("")
                        Text("Current")
                        Text("Next")
                    }
                    Section() {
                        Text("Quickness")
                        Toggle("", isOn: $adminReview.quicknessCurr)
                        Toggle("", isOn: $adminReview.quicknessNext)
                        Text("Agility")
                        Toggle("", isOn: $adminReview.agilityCurr)
                        Toggle("", isOn: $adminReview.agilityNext)
                        Text("Balance")
                        Toggle("", isOn: $adminReview.balanceCurr)
                        Toggle("", isOn: $adminReview.balanceNext)
                    }
                    Section() {
                        Text("Reaction Time")
                        Toggle("", isOn: $adminReview.reactionTimeCurr)
                        Toggle("", isOn: $adminReview.reactionTimeNext)
                        Text("Aerobic")
                        Toggle("", isOn: $adminReview.aerobicCurr)
                        Toggle("", isOn: $adminReview.aerobicNext)
                        Text("Strategy")
                        Toggle("", isOn: $adminReview.strategyCurr)
                        Toggle("", isOn: $adminReview.strategyNext)
                    }
                    Section() {
                        Text("Mental Toughness")
                        Toggle("", isOn: $adminReview.mentalToughnessCurr)
                        Toggle("", isOn: $adminReview.mentalToughnessNext)
                    }
                }
                .font(.system(size: 20))
                .padding(.top, 15)
                .padding(.bottom, 15)
            }
            Section(header: Text("Notes")) {
                TextField("Lesson Notes", text: $adminReview.notes)
                    .font(.system(size: 20))
            }
            Button(action: {
                publishAdminReviewForUser()
                
            }) {
                HStack {
                    Spacer()
                    Text("Submit")
                    Spacer()
                }
            }
        } // End of Form
        .alert(isPresented: $showFormAlert, content: { self.formAlert })
        .navigationBarTitle("New Lesson Review", displayMode: .inline)
        .toggleStyle(CheckboxToggleStyle())
        .labelsHidden()
        
    } // End of body
    
    var formAlert: Alert {
        Alert(title: Text(formAlertTitle),
              message: Text(formAlertTitle),
              dismissButton: .default(Text("OK")) {
                self.presentationMode.wrappedValue.dismiss()
              } )
    }
    
    func publishAdminReviewForUser() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/YYYY"
        
        let lessonReview = LessonReview(
            id: UUID().uuidString,
            lessonDate: dateFormatter.string(from: self.date),
            submittedBy: "\(userData.currentUser.firstName) \(userData.currentUser.lastName)",
            admin: self.adminReview,
            client: nil,
            isComplete: false)
        
        let lessonReviewDict = lessonReview.toDictionary() // Gets a dict representaiton of the review
        database.child("reviews").child(self.user.id).child(lessonReview.id).setValue(lessonReviewDict) {
            (error: Error?, ref:DatabaseReference) in

            if let error = error {
                print("Data could not be saved: \(error)")
                formAlertTitle = "Uh Oh"
                formAlertDescription = "Data could not be saved. Please try again"
                showFormAlert = true
            }
            else {
                let requestGroup = DispatchGroup()
                print("Lesson review saved successfully")
                formAlertTitle = "Review Sent"
                formAlertDescription = "Your review was successfuly published"
                
                requestGroup.enter()
                database.child("review_ids").child(self.user.id).child(lessonReview.id).setValue(lessonReview.isComplete) {
                    (error: Error?, ref:DatabaseReference) in
                    
                    if error != nil {
                        print("*** Review_ids was NOT updated ***")
                    }
                    else {
                        print("*** Review_ids was updated ***")
                        
                    }
                    requestGroup.leave()
                }
                
                requestGroup.notify(queue: .main) {
                    showFormAlert = true

                } // End of completion block
            } // End of condition
        } // End of completion block
    } // End of function
}

struct CheckboxToggleStyle: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        return HStack {
            configuration.label
            Image(systemName: configuration.isOn ? "checkmark.square" : "square")
                .resizable()
                .frame(width: 22, height: 22)
                .onTapGesture { configuration.isOn.toggle() }
                .foregroundColor(configuration.isOn ? .accentColor : .white)
        }
    }
}

