//
//  ClientDetails.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/19/21.
//

import SwiftUI

struct ClientDetails: View {
    
    let user: DbUser
    
    var body: some View {
        
        Form {
            Section(header: Text("USTA ID")) {
                Text(user.ustaNumber)
                    .multilineTextAlignment(.leading)
            }
            Section(header: Text("Universal Tennis Rating")) {
                Text(user.utrNumber)
                    .multilineTextAlignment(.leading)
                
            }
            Section(header: Text("Date of Birth")) {
                Text(user.birthday)
                    .multilineTextAlignment(.leading)
            }
            Section(header: Text("Phone Number")) {
                Link(destination: URL(string: "tel://\(user.phone)")!) {
                    HStack {
                        Image(systemName: "phone")
                            .foregroundColor(.accentColor)
                        Text("\(formatPhone(phoneNumber: user.phone) ?? "Unavailable")")
                            .multilineTextAlignment(.leading)
                    }
                }
            }
            
            Section(header: Text("Email")) {
                Link(destination: URL(string: "mailto:\(user.email)")!) {
                    HStack {
                        Image(systemName: "envelope")
                            .foregroundColor(.accentColor)
                        Text("\(user.email)")
                            .multilineTextAlignment(.leading)
                    }
                }
            }
            
            Section(header: Text("Lesson Reviews")) {
                
                NavigationLink(destination: AdminReviewsList(user: self.user)) {
                    HStack {
                        Text("View \(user.firstName)'s Reviews")
                            .multilineTextAlignment(.leading)
                    }
                }
            }
        
        }// end of Form
        .font(.system(size: 14))
        .navigationBarTitle(Text("\(user.firstName) \(user.lastName)"))
        
    }// end of body
}

