//
//  ClientItem.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/19/21.
//

import SwiftUI

struct ClientItem: View {
    
    let user: DbUser
    @EnvironmentObject var userData: UserData

    var body: some View {
        HStack {
            if(user.profilePic == Image(systemName: "person.circle")) {
                Image("TriTennisLogo")
                    .resizable()
                    .cornerRadius(15)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 65.0)
                    .padding(5)
            }
            else {
                user.profilePic
                    .resizable()
                    .cornerRadius(15)
                    .overlay(RoundedRectangle(cornerRadius: 15)
                                .stroke(backgroundGradient, lineWidth: 3))
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 65.0)
                    .padding(5)
            }
            
            VStack(alignment: .leading) {
                HStack {
                    Text("\(user.firstName) \(user.lastName)")
                        .font(.system(size: 18))
                        .foregroundColor(.accentColor)
                    
                }
                Text("Last Lesson: \(mostRecentReview())")
                    .font(.system(size: 12))
                    .opacity(0.6)
            }
            Spacer()
            VStack {
                if self.hasIncompleteReviews() {
                    Image(systemName: "exclamationmark.circle")
                        .imageScale(.medium)
                        .foregroundColor(.accentColor)
                        .padding(.trailing, 20)
                        .font(Font.title2)
                }
            }
        }
    }
    
    func hasIncompleteReviews() -> Bool {
        if let userReviews = userData.allReviews[user.id] {
            for (_, lessonReview) in userReviews {
                if !lessonReview.isComplete {
                    return true
                }
            }
        }
        return false
    }
    
    func mostRecentReview() -> String {
        if let reviews = userData.allReviews[user.id] {
            let reviewsList = Array(reviews.values)
            
            if reviewsList.isEmpty {
                return "No recent reviews"
            }
            else {
                var mostRecent = reviewsList[0].lessonDate
                
                reviewsList.forEach { (aReview) in
                    if aReview.lessonDate > mostRecent {
                        mostRecent = aReview.lessonDate
                    }
                }
                return mostRecent
            }
        }
        else {
            return "No recent reviews"
        }
        
    }

}
