//
//  News.swift
//  PocketTennis
//
//  Created by ZhangFan on 10/6/20.
//

import SwiftUI

struct Home: View {
    @EnvironmentObject var userData: UserData
    @Binding var tabSelection: Int
    
    var body: some View {
        NavigationView {
            ZStack {
                backgroundGradient
                    .edgesIgnoringSafeArea(.all)
                ScrollView {
                //VStack {
                    VStack {
                        Text("Home")
                            .font(.system(size: 35))
                        CurrentDate
                            .font(.system(size: 15))
                    }
                    .foregroundColor(.white)
                    .padding()
                                        
                    HStack(alignment: .top) {
                        Spacer()
                        if (userData.currentUser.isAdmin) {
                            Button(action: {
                                self.tabSelection = 2
                            }) {
                                Icon(imageName: "MyReviews", title: "Clients")
                            }
                        }
                        else {
                            Button(action: {
                                self.tabSelection = 2
                            }) {
                                Icon(imageName: "MyReviews", title: "My Reviews")
                            }
                        }
                        Spacer(minLength: 20)
                        NavigationLink(destination: WebView(showURL: "https://www.facebook.com/TriAdventureTennis/").navigationBarTitle(Text("TriAdventure Tennis"), displayMode: .inline))
                        {
                            VStack{
                                ZStack {
                                    Rectangle()
                                        .frame(width: 140, height: 140, alignment: .center)
                                        .foregroundColor(.FacebookColor)
                                        .cornerRadius(30)
                                        .shadow(radius: 10)
                                    VStack {
                                        Spacer()
                                        Image("TriTennisLogo")
                                            .renderingMode(.original)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 80, height: 80, alignment: .top)
                                            .shadow(radius: 10)
                                            .opacity(0.8)
                                        Text("Facebook")
                                            .font(.system(size: 20))
                                            .bold()
                                            .foregroundColor(.white)
                                            .opacity(0.8)
                                            .shadow(color: .black, radius: 1)
                                            .frame(width: 110, height: 27, alignment: .bottom)
                                        Spacer()
                                    }
                                }
                            }
                        }
                        Spacer()
                    } // End of HStack
                    Spacer(minLength: 20)
                    NavigationLink(destination:TennisBasics().navigationBarTitle(Text("Rackets"), displayMode: .inline))
                    {
                        WideIcon(imageName: "ChooseRacket", title: "Choosing a Racket")
                    }
                    Spacer(minLength: 20)
                    HStack {
                        NavigationLink(destination:
                            AboutAnne())
                        {
                            Icon(imageName: "AboutAnne", title: "About Me")
                        }
                        Spacer(minLength: 30)
                        NavigationLink(destination: WebView(showURL: "https://www.usatoday.com/sports/tennis/").navigationBarTitle(Text("USA TODAY"), displayMode: .inline))
                        {
                            Icon(imageName: "USAToday", title: "USA Today")
                        }
                    }
                    .frame(width: 310, height: 155, alignment: .center)
                    Spacer(minLength: 20)
                    NavigationLink(destination: WebView(showURL: "https://www.atptour.com/en/").navigationBarTitle(Text("ATP Tour"), displayMode: .inline))
                    {
                        WideIcon(imageName: "ATPTour", title: "ATP Tour")
                    }
                } // end of scrollview
            } // End of ZStack
            .navigationBarHidden(true)
        } // End of NavigationView
    } // End of body
    
    var width: CGFloat {
            if UIDevice.current.userInterfaceIdiom == .phone {
                return UIScreen.main.bounds.width * 0.9
            } else {
                return UIScreen.main.bounds.width * 0.4
            }
    }
    var height: CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
                return UIScreen.main.bounds.height * 0.9
            } else {
                return UIScreen.main.bounds.height * 0.4
            }
    }
    
    var CurrentDate: Text {
        // Obtain current date and time
        let currentDateAndTime = Date()

        // Create an instance of DateFormatter()
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .full
        let dateAndTimeString = dateFormatter.string(from: currentDateAndTime).dropLast(6).uppercased()
        return Text(verbatim: dateAndTimeString)
    }
} // End of News Struct

extension Color {
    static let FacebookColor = Color("FacebookColor")
}
