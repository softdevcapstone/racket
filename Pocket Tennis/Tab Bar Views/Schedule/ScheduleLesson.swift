//
//  ScheduleLesson.swift
//  Racket
//
//  Created by Harlan  Reese Pounders on 3/29/21.
//

import SwiftUI

struct ScheduleLesson: View {
    var body: some View {
        NavigationView {
            WebView(showURL: "https://triadventuretennis.as.me/schedule.php")
                .frame(width: UIScreen.main.bounds.width)
                .navigationBarTitle("Schedule New Lesson", displayMode: .inline)
        }
        
    }
}

struct ScheduleLesson_Previews: PreviewProvider {
    static var previews: some View {
        ScheduleLesson()
    }
}
