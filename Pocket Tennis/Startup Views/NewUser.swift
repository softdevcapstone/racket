//
//  NewUser.swift
//  Racket
//
//  Created by Ally Johnson on 3/10/21.
//

import SwiftUI
import UIKit
import FirebaseAuth
import Firebase

struct NewUser: View {
    
    
    @EnvironmentObject var userData: UserData
    @State private var accountAlertTitle = ""
    @State private var accountAlertMessage = ""
    
    @State private var missingDataTitle = ""
    @State private var missingDataMessage = ""
    
    @State private var accountCreated = false
    @State private var showAccountAlert = false
    @State private var showMissingInputDataAlert = false

    @State private var email = ""
    @State private var password = ""
    @State private var firstName = ""
    @State private var lastName = ""
    @State private var birthDate = Date()
    @State private var phoneNumber = ""
    @State private var ustaNumber = ""
    @State private var utrNumber = ""

    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()




    @State private var notes = "Enter Notes"
    @State private var showPassword = false
    @Environment(\.presentationMode) var presentationMode
    
    let colors = [Color.red, Color.purple]

    var body: some View {
       
        
        ZStack {
            VStack {
                Form {
                    
                    Section(header: Text("First Name")) {
                        HStack {
                            TextField("Enter First Name", text: $firstName)
                                .disableAutocorrection(true)
                                .autocapitalization(.words)
                                .padding(.all)
                           
                            // Button to clear the text field
                            Button(action: {
                                self.firstName = ""
                            }) {
                                Image(systemName: "clear")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                           
                        }   // End of HStack
                    }
                    Section(header: Text("Last Name")) {
                        HStack {
                            TextField("Enter Last Name", text: $lastName)
                                .padding(.all)
                                .disableAutocorrection(true)
                                .autocapitalization(.words)
                           
                            // Button to clear the text field
                            Button(action: {
                                self.lastName = ""
                            }) {
                                Image(systemName: "clear")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                           
                        }   // End of HStack
                    }
                    Section(header: Text("Phone Number")) {
                        HStack {
                            TextField("Enter Phone Number", text: $phoneNumber)
                                .padding(.all)
                                .disableAutocorrection(true)
                                .autocapitalization(.none)
                           
                            // Button to clear the text field
                            Button(action: {
                                self.phoneNumber = ""
                            }) {
                                Image(systemName: "clear")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                           
                        }   // End of HStack
                    } // End of section
                    Section(header: Text("Tennis Association ID")) {
                        HStack {
                            TextField("Enter Tennis Association ID", text: $ustaNumber)
                                .padding(.all)
                                .disableAutocorrection(true)
                                .autocapitalization(.none)
                           
                            // Button to clear the text field
                            Button(action: {
                                self.ustaNumber = ""
                            }) {
                                Image(systemName: "clear")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                           
                        }   // End of HStack
                    }
                    
                    Section(header: Text("Universal Tennis Rating")) {
                        HStack {
                            TextField("Enter Rating", text: $utrNumber)
                                .padding(.all)
                                .disableAutocorrection(true)
                                .autocapitalization(.none)
                           
                            // Button to clear the text field
                            Button(action: {
                                self.utrNumber = ""
                            }) {
                                Image(systemName: "clear")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                           
                        }   // End of HStack
                    }

                    
                    Section(header: Text("Account Email")) {
                        HStack {
                            TextField("Enter Account Email", text: $email)
                                .padding(.all)
                                .disableAutocorrection(true)
                                .autocapitalization(.none)
                           
                            // Button to clear the text field
                            Button(action: {
                                self.email = ""
                            }) {
                                Image(systemName: "clear")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                           
                        }   // End of HStack
                    }
                    Section(header: Text("Birthday")) {
                        DatePicker("", selection: $birthDate, in: ...Date(), displayedComponents: .date)
                        .datePickerStyle(GraphicalDatePickerStyle())
                    } // End of section

                    Section(header: Text("Account Password")) {
                        HStack {
                            if(showPassword){
                                TextField("Enter Account Password", text: $password)
                                    .padding(.all)
                                    .disableAutocorrection(true)
                                    .autocapitalization(.none)
                            }
                            else{
                                SecureField("Enter Account Password", text: $password)
                                    .padding(.all)
                                    .disableAutocorrection(true)
                                    .autocapitalization(.none)
                            }
                           
                            // Button to clear the text field
                            Button(action: {
                                self.showPassword.toggle()
                            }) {
                                Image(systemName: self.showPassword ? "eye" : "eye.fill")
                                    .imageScale(.medium)
                                    .font(Font.title.weight(.regular))
                            }.buttonStyle(PlainButtonStyle())
                            .foregroundColor(.accentColor)
                            .padding()
                           
                        }   // End of HStack
                    }
                    Button(action: {
                        if self.inputDataValidated() {
                            self.addNewAccount()
                        } else {
                            print("Missing data")
                            self.showMissingInputDataAlert = true
                        }
                    }) {
                        HStack {
                            Spacer()
                            Text("Create Account")
                            Spacer()
                        }
                    }
                    .foregroundColor(.accentColor)
                    .alert(isPresented: $showMissingInputDataAlert, content: { self.missingInputDataAlert })
                } // End of Form
                .navigationBarTitle("Create New Account")

                
                
            }// End of VStack
            .alert(isPresented: $showAccountAlert, content: { self.accountAlert })
            
        }  //End of ZStack
            
    }
    
    /**
     Shows the missing information error message if a user doesn't complete all fields and tries to add the account
     */
    var missingInputDataAlert: Alert {
        Alert(title: Text(self.missingDataTitle),
              message: Text(self.missingDataMessage),
              dismissButton: .default(Text("OK")) )
    }
    
    /**
     Shows a success message when a new account is created
     */
    var accountAlert: Alert {
        Alert(title: Text(self.accountAlertTitle),
              message: Text(self.accountAlertMessage),
              dismissButton: .default(Text("OK")) {
                if self.accountCreated {
                    self.presentationMode.wrappedValue.dismiss()
                }
              })
    }
    
    /**
     Checks if the user entered their first name, last name, email, and password. If one of these field is not populated an error message pops up asking the user to make sure all fields are populated
     */
    func inputDataValidated() -> Bool {
       
        if (firstName.isEmpty || lastName.isEmpty || email.isEmpty || password.isEmpty
            || utrNumber.isEmpty || ustaNumber.isEmpty || phoneNumber.isEmpty) {
            
            missingDataTitle = "Missing Fields"
            missingDataMessage = "Please make sure you have filled out all fields"
            
            return false
        }
        else if (!CharacterSet(charactersIn: firstName).isSubset(of: CharacterSet.letters.union(CharacterSet(charactersIn: " '-")))) {
            
            missingDataTitle = "Invalid First Name"
            missingDataMessage = "Please make sure your first name only contains letters, hyphens, spaces, and apostrophes"
            
            return false
        }
        else if (!CharacterSet(charactersIn: lastName).isSubset(of: CharacterSet.letters.union(CharacterSet(charactersIn: " '-")))) {
            
            missingDataTitle = "Invalid Last Name"
            missingDataMessage = "Please make sure your last name only contains letters, hyphens, spaces, and apostrophes"
            
            return false
        }
        else if (!CharacterSet(charactersIn: phoneNumber).isSubset(of: CharacterSet.decimalDigits)
                 || formatPhone(phoneNumber: phoneNumber) == nil
        ) {
            missingDataTitle = "Invalid Phone Number"
            missingDataMessage = "Please make sure your phone number contains only numbers and spaces and is at most 11 digits"
            return false
        }
        else if (!CharacterSet(charactersIn: ustaNumber).isSubset(of: CharacterSet.decimalDigits)) {
            missingDataTitle = "Invalid USTA ID"
            missingDataMessage = "Please make sure your USTA ID contains only numbers"
            return false
        }
        else if (!CharacterSet(charactersIn: utrNumber).isSubset(of: CharacterSet(charactersIn: "0123456789.")) || !isValidUTR(utr: utrNumber)) {
            missingDataTitle = "Invalid UTR"
            missingDataMessage = "Please make sure your Universal Tennis Rating contains only a decimal value between 1.00 and 16.50"
            return false
        }
        return true
    }
    
    
    /**
    This is the function that needs to be  implemented for the backend for adding a new user to the database
     */
    func addNewAccount() {
            
            // Register new user via Firebase - Pau
            Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
                
                if let error = error as NSError? {
                    if let errCode = AuthErrorCode(rawValue: error.code) {
                        switch errCode {
                        case .invalidEmail:
                            self.accountAlertTitle = "Invalid Email"
                            self.accountAlertMessage = "The email you provided was invalid or otherwise malformed"
                        case .emailAlreadyInUse:
                            self.accountAlertTitle = "Email In Use"
                            self.accountAlertMessage = "An account with that email already exists!"
                        case .weakPassword:
                            self.accountAlertTitle = "Weak Password"
                            self.accountAlertMessage = "Your password is too weak. \(error.userInfo[NSLocalizedFailureReasonErrorKey] ?? "Strenghten your password by adding length, symbols, and variable casing")"
                        default:
                            self.accountAlertTitle = "Invalid Error"
                            self.accountAlertMessage = "This is the defaul error handling case for account creation"
                        }
                        self.showAccountAlert = true
                    }
                    else {
                        self.accountAlertTitle = "Error"
                        self.accountAlertMessage = "An unexpected error occurred, please try again"
                    }
                    
                }
                else {
                    if Auth.auth().currentUser != nil {
                        self.accountCreated = true
                        let user = Auth.auth().currentUser
                        if let user = user {
                            //let uid = user.uid
                            let birthDateString = dateFormatter.string(from: birthDate)
                            
                            let userDictionary = [
                                "id": user.uid,
                                "phone" : phoneNumber,
                                "ustaNumber" : ustaNumber,
                                "utrNumber" : roundStringNumToTwoPlaces(utr: utrNumber) ?? "Unavailable",
                                "email": email,
                                "firstName": firstName,
                                "lastName": lastName,
                                "isAdmin" : false,
                                "birthday" : birthDateString
                            ] as [String : Any]
                            
                            let requestGroup = DispatchGroup()
                            requestGroup.enter()
                            requestGroup.enter()
                            database.child("users").child(user.uid).setValue(userDictionary) {
                                (error:Error?, ref:DatabaseReference) in
                                if error != nil {
                                    print("ERROR" + (error?.localizedDescription)!)
                                } else {
                                    self.accountAlertTitle = "Account Created!"
                                    self.accountAlertMessage = "Your new account has been created."
                                    //Show the alert when a new account is added
                                    
                                    database.child("user_ids").child(user.uid).setValue(true) {
                                        (error:Error?, ref:DatabaseReference) in
                                        if error != nil {
                                            print("ERROR" + (error?.localizedDescription)!)
                                        } else {
                                           print("*** User_ids was successfully updated")
                                        }
                                        requestGroup.leave()
                                      }
                                }
                                requestGroup.leave()
                            }
                            
                            requestGroup.notify(queue: .main) {
                                self.showAccountAlert = true
                            }
                        }
                        
                        
                    } else {
                      // No user is signed in.
                      // ...
                    }
                    
                } // End of error check
                
            } // End of create user completion block
        
        }
    
    func roundStringNumToTwoPlaces(utr: String) -> String? {
        
        if let doubleUtr = Double(utr) {
           return String(format: "%.2f", doubleUtr)
        }
        else {
            return nil
        }
       
    }
    
    func isValidUTR(utr: String) -> Bool {
        if let doubleUtr = Double(utr) {
            return (1.00 <= doubleUtr) && (doubleUtr <= 16.50)
        }
        else {
            return false
        }
    }
}
