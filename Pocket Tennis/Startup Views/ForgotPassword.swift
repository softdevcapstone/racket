//
//  ForgotPassword.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 3/10/21.
//

import SwiftUI
import FirebaseAuth
import Firebase

struct ForgotPassword: View {
    
    @State private var enteredEmail = ""
    
    @State private var showResetAlert = false
    @State private var success = false
    @State private var resetAlertTitle = ""
    @State private var resetAlertMessage = ""

    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            backgroundGradient
                .ignoresSafeArea(.all)
            
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    TextField("Enter your account email", text: $enteredEmail)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 300, height: 36)
                        .padding(.top, 40)
                    
                    Button(action: {
                        passwordReset(email: enteredEmail)
                    }
                    ) {
                        Text("Reset Password")
                            .font(Font.subheadline.weight(.semibold))
                            .frame(width: 150, height: 36, alignment: .center)
                            .foregroundColor(.white)
                            .background(
                                RoundedRectangle(cornerRadius: 16)
                                    .foregroundColor(.accentColor)
                                    .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                            )
                    }
                    .padding(.top, 15)
                }   // End of VStack
                .navigationBarTitle("Reset Password")
                .alert(isPresented: $showResetAlert, content: { self.resetAlert })
            }

        }   // End of ZStack
    }
    
    /*
     Sends user an email with a link to reset their password outside the app.
     Update success/error message.
     */
    func passwordReset(email: String) {
        Auth.auth().sendPasswordReset(withEmail: enteredEmail) { (error) in
            if error == nil {
                resetAlertTitle = "Email Sent"
                resetAlertMessage = "A password reset email was sent to the provided email"
                success = true
            } else {
                resetAlertTitle = "Uh Oh"
                resetAlertMessage = "The password reset email could not be sent. Please check that your email is valid and try again"
            }
            self.showResetAlert = true
        }
    }
    
    var resetAlert: Alert {
        Alert(title: Text(self.resetAlertTitle),
              message: Text(self.resetAlertMessage),
              dismissButton: .default(Text("OK")) {
                if success {
                    self.presentationMode.wrappedValue.dismiss()
                }
        })
    }
}

struct ForgotPassword_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPassword()
    }
}
