//
//  LoginView.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 3/10/21.
//  Authors: Pau Lleonart Calvo, Jake Barker
//


import LocalAuthentication
import AVFoundation
import SwiftUI
import Firebase

struct LoginView: View {
    
    @EnvironmentObject var userData: UserData
    
    @State private var enteredEmail = ""
    @State private var enteredPassword = ""
    @State private var showAlert = false
    @State private var alertTitle = ""
    @State private var alertMessage = ""
    
    var body: some View {
        NavigationView{
            ZStack {
                backgroundGradient
                    .edgesIgnoringSafeArea(.all)
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        Text("Pocket Tennis")
                            .font(Font.title.bold())
                            .foregroundColor(.white)
                        Image("TriTennisLogo")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 150, maxWidth: 150)
                            .padding(.horizontal, 30)
                            .padding(.top, 25)
                            .padding(.bottom, 50)
                            .foregroundColor(.white)
                            .shadow(radius: 15)
                        
                        TextField("Email", text: $enteredEmail)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .frame(width: 300, height: 36)
                            
                        
                        SecureField("Password", text: $enteredPassword)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .frame(width: 300, height: 36)
                            .padding()
                        
                        HStack{
                            // Login Button
                            Spacer()

                            Button(action: {
                                authorizeUser(email: self.enteredEmail, password: self.enteredPassword)
                            }
                            ) {
                                Text("Login")
                                    .font(Font.subheadline.weight(.semibold))
                                    .frame(width: 100, height: 36, alignment: .center)
                                    .foregroundColor(.white)
                                    .background(
                                        RoundedRectangle(cornerRadius: 16)
                                            .foregroundColor(.accentColor)
                                            .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                    )
                                    
                            }
                            .padding(.bottom, 10)
                            .padding(.trailing, 5)

                            NavigationLink(destination: ForgotPassword()){
                                Text("Forgot Password")
                                    .font(Font.subheadline.weight(.semibold))
                                    .frame(width: 180, height: 36, alignment: .center)
                                    .foregroundColor(.white)
                                    .background(
                                        RoundedRectangle(cornerRadius: 16)
                                            .foregroundColor(.accentColor)
                                            .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                                    )
                            }.buttonStyle(PlainButtonStyle()) //End of Navigationlink
                            .padding(.bottom,  10)
                            
                            Spacer()
                            
                        } //End of HStack
                        
                        HStack{
                            Text("New User? ")
                                .foregroundColor(.white)
                            NavigationLink(destination: NewUser()){
                                Text("Sign up")
                                    .foregroundColor(.white)
                                    .underline()
                                    .fontWeight(.bold)
                                    .shadow(radius: 5)
                            }
                        }
                        .padding(.top, 5)
                    }   // End of VStack
                    
                }   // End of ScrollView
            }   // End of ZStack
            .alert(isPresented: $showAlert, content: {
                    self.loginAlert})
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }   // End of body
    
    /*
     ------------------------------
     MARK: - Alerts
     ------------------------------
     */
    var loginAlert: Alert {
        Alert(title: Text(alertTitle),
              message: Text(alertMessage),
              dismissButton: .default(Text("OK")) )
    }
    
    func authorizeUser(email: String, password: String) {
        
        if (email.isEmpty || password.isEmpty) {
            alertTitle = "Missing Data"
            alertMessage = "Please make sure you have provided an email and password"
            showAlert = true
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if let error = error as NSError? {
                print("*** \(error.localizedDescription) ***")
            switch AuthErrorCode(rawValue: error.code) {
            case .operationNotAllowed:
                print("Operation not allowed error")
                alertTitle = "Operation Not Allowed"
                alertMessage = "Email and password accounts are not enabled"
            case .userDisabled:
                print("User disabled error")
                alertTitle = "User Disabled"
                alertMessage = "This user's account has been disabled. Contact administrators if this is a mistake"
            case .wrongPassword:
                print("Wrong password error")
                alertTitle = "Incorrect"
                alertMessage = "Email or password was incorrect. Please check your credentials"
            case .invalidEmail:
                print("Invalid email error")
                alertTitle = "Invalid Email"
                alertMessage = "This email was either invalid or otherwise malformed"
            default:
                print("Could not log in error")
                alertTitle = "Could Not Login"
                alertMessage = "No account exists with the provided credentials"
            }
                print("Error logging in")
                self.showAlert = true
          } else {
            print("User signs in successfully")
            
            if let user = Auth.auth().currentUser {
                //Populate data for admin or clients
                populateNecessaryDataOnLaunch(userData: userData, userID: user.uid)
            }
          }
        }
    }// End of authorize user
    
} // End of LoginView


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

