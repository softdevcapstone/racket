//
//  MainView.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 3/10/21.
//

import SwiftUI
import FirebaseAuth
import Firebase

struct MainView: View {
    
    init() {
        UITabBar.appearance().backgroundColor = secondColor
    }
    
    @EnvironmentObject var userData: UserData
    
    @State private var tabSelection = 1
    
    var body: some View {
        TabView(selection: $tabSelection) {
            Home(tabSelection: $tabSelection).tabItem {
                Image(systemName: "house")
                Text("Home")
            }.tag(1)
            if (userData.currentUser.isAdmin) {
                ClientsList().tabItem {
                    Image(systemName: "rectangle.stack.person.crop")
                    Text("Clients")
                }.tag(2)
            } else {
                ClientReviewList().tabItem {
                    Image(systemName: "list.dash")
                    Text("My Reviews")

                }.tag(2)
            }
            ScheduleLesson().tabItem {
                Image(systemName: "calendar.badge.plus")
                Text("Schedule")
            }.tag(3)
            Profile().tabItem {
                Image(systemName: "person")
                Text("Profile")
            }.tag(4)
        }   // End of TabView
        .font(.headline)
        .imageScale(.large)
        .font(Font.title.weight(.regular))
    } //End of body
}
