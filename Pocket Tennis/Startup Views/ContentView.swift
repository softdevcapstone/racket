//
//  ContentView.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 2/10/21.
//


import SwiftUI

struct ContentView : View {
    
    // Subscribe to changes in UserData
    @EnvironmentObject var userData: UserData
    
    // UserDefaults key to check
    // UserDefaults.standard.bool(forKey: "loggedIn")
    
    var body: some View {
        if userData.userAuthenticated {
            return AnyView(MainView())
        } else {
            return AnyView(LoginView())
        }
    }
}
