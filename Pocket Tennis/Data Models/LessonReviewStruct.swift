//
//  LessonReviewStruct.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/18/21.
//

import SwiftUI

struct LessonReview: Identifiable {
    
    var id: String
    var lessonDate: String
    var submittedBy: String
    var admin: AdminReview?
    var client: ClientReview?
    var isComplete: Bool
    
    func toDictionary() -> [String: Any] {
        
        var dict = [String: Any]()
        
        dict["id"] = id
        dict["lessonDate"] = lessonDate
        dict["submittedBy"] = submittedBy
        
        if admin != nil {
            dict["admin"] = admin?.toDictionary()
        }
        
        if client != nil {
            dict["client"] = client?.toDictionary()
        }
        
        return dict
    }
}
