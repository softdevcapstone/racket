//
//  UserStruct.swift
//  Racket
//
//  Created by Pau Lleonart Calvo on 4/12/21.
//

import SwiftUI

struct DbUser: Identifiable {
    @EnvironmentObject var userData: UserData

    
    var id: String
    var firstName: String
    var lastName: String
    var birthday: String
    var phone: String
    var email: String
    var ustaNumber: String
    var utrNumber: String
    let isAdmin: Bool
    var profilePic: Image
    
    init(isAdmin: Bool) {
        self.id = ""
        self.firstName = ""
        self.lastName =  ""
        self.birthday = ""
        self.phone = ""
        self.email = ""
        self.ustaNumber = ""
        self.utrNumber = ""
        self.isAdmin = isAdmin
        self.profilePic = Image(systemName: "person.circle")
    }
    
}
